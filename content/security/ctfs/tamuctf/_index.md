+++
title = "TAMUctf"
+++

TAMUctf is a yearly competition hosted and run by TAMU students. I have the pleasure to be the current development lead
for the group and will be posting some things related to its development cycle and infrastructure (no spoilers!).

<br>
<br>
<br>
<br>
<br>

[![tamuctf logo](logo.png)](https://tamuctf.com)