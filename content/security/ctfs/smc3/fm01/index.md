+++
title = "fe01"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/fm01.zip and use the included word list to extract the flag.

We're given a zip file, a wordlist, and a password policy for mangling to try it against.

Using john (again) because we're lazy, but with `--rules` enabled:
```bash
zip2john flag.zip > flag.hash
john --wordlist=wordlist.txt --rules flag.hash
john --show flag.hash
```
```text
flag.zip/flag.txt:Cartoon4:flag.txt:flag.zip::flag.zip
```

Using this password `Cartoon4` on the zip file, we can open `flag.txt`:
```text
Flag: mangLINg-w0RDs-62517
```

Still overkill, whoop!