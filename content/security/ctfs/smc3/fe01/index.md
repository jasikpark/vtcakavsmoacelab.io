+++
title = "fe01"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/fe01.zip and find a way to extract the flag using the provided word list.

We're given a zip file and a wordlist to try it against.

Using john because we're lazy:
```bash
zip2john flag.zip > flag.hash
john --wordlist=wordlist.txt flag.hash
john --show flag.hash
```
```text
flag.zip/flag.txt:digital:flag.txt:flag.zip::flag.zip
```

Using this password `digital` on the zip file, we can open `flag.txt`:
```text
Flag: z1P-Cr4CK-0910
```

Little bit overkill, but eh.