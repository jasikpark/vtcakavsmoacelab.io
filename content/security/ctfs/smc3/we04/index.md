+++
title = "we04"
+++

Description:

>Visit the news site at https://ggcs-we04.allyourbases.co and see if you can find a way to access the article without subscribing.

"Breaking News: Robots gain sentience, take over world!"

Seems like fake news to me, but let's read it anyways. Inspect element finds this comment:
```html
<!--TODO: SEO is important to us, particularly Google's results are important!-->
```

[A quick Google Dork shall suffice.](https://www.google.com/search?q=allintext%3A%22Breaking+News%3A+Robots+gain+sentience%2C+take+over+world%21%22)
Blurb from Google shows: `Flag: CrawlING-So-SlOwLY-8199`