+++
title = "fe03"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/fe03.zip and find a way to retrieve the flag from the gif.

First thing I always do with forensics challenges is binwalk:
```bash
binwalk --dd=".*" -M allyourbase.gif 
```
```text
Scan Time:     2020-06-21 10:21:59
Target File:   /home/addisoncrump/Dokumente/SMC3/fe03/allyourbase.gif
MD5 Checksum:  e04de33e00709777bff2bf598d590b8d
Signatures:    391

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             GIF image data, version "89a", 248 x 248
1563852       0x17DCCC        ELF, 32-bit LSB shared object, Intel 80386, version 1 (SYSV)
```

Hello, ELF. One chmod later (and quick inspection with Ghidra to make sure it's not `rm -rf /`):
```bash
_allyourbase.gif.extracted/17DCCC
```
```text
Flag: EMbEddedFiLEz_0819
```
