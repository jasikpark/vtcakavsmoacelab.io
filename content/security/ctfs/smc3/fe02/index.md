+++
title = "fe02"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/fe02.zip and see if you can recover the redacted document to get the flag.

Funnily enough, my PDF viewer (Okular) just allows me to copy that text. Oops.

One copy/paste later...
```text
Flag: cAnYOuReALLYSEEme-2322
```