+++
title = "Rust"
+++

Rust is a programming language for the future.

## What's that?

Rust is a really powerful language with a lot of really cool features and a
wonderful community. It might not be the best language for new programmers, but
it's relatively simple and can do basically anything you want.
[Try it out!](https://play.rust-lang.org)
