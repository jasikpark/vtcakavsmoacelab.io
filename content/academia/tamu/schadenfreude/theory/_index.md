+++
title = "Theory"
weight = 2
sort_by = "weight"
date = 2021-01-22
+++

This section contains the theory utilised in
[Schadenfreude: Resurrection](https://gitlab.com/addison-and-teddy/schadenfreude/-/tree/resurrection). This is
ultimately the core of how Schadenfreude: Resurrection works and you should certainly read this, regardless of whether
you intend to contribute or simply use Schadenfreude. 

I recommend starting with [mixed flow graphs](mixed-flow-graph), then [converting them to SMT statements](mfg-to-smt).
More coming shortly