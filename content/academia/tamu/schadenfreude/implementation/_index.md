+++
title = "Implementation"
weight = 3
date = 2021-01-22
+++

This section contains details regarding the implementation of
[Schadenfreude: Resurrection](https://gitlab.com/addison-and-teddy/schadenfreude/-/tree/resurrection). I strongly
recommend reading through the [theory](../theory) before going through this section as I repeatedly refer back to
items in that section without a thorough explanation.