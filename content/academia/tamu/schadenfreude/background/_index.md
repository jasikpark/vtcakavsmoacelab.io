+++
title = "Background"
weight = 1
date = 2021-01-22
+++

This section contains the background surrounding the theory of
[Schadenfreude: Resurrection](https://gitlab.com/addison-and-teddy/schadenfreude/-/tree/resurrection). I strongly
recommend that, should you be unfamiliar with the topics in this section, review them here and follow the links I've
placed on each page BEFORE reading through the [theory](../theory).